
class Player:
    playerName = ''

    def __init__(self, name):
        self.playerName = name
        self.hand = []

    def addCard(self, card):
        self.hand.append(card)

    def playCard(self):
        if len(self.hand) > 0 :
            return self.hand.pop(0)

    def addWin(self, cards):
        for card in cards:
            self.hand.append(card)