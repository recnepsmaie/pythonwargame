from Card import Card
import random

class Deck:
    cards = []
    suits = ['Heart', 'Spade', 'Diamond', 'Club']
    valueFace = {2:'2',3:'3',4:'4',5:'5',6:'6',7:'7',8:'8',9:'9',10:'10',11:'J',12:'Q',13:'K',14:'A'}

    def __init__(self):
        for suit in self.suits:
            for key in self.valueFace.keys():
                self.cards.append( Card(suit, self.valueFace.get(key), key ) )

        self.shuffleCards()

    def getCards(self):
        return self.cards

    def dealCard(self):
        return self.cards.pop()

    def shuffleCards(self):
        random.shuffle(self.cards)