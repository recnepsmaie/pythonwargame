import collections

class BattleGround:

    def __init__(self):
        self.combatants = {}
        self.playedCards = []
    def addCombatant(self, player):
        card = player.playCard()
        if( card ):
            self.playedCards.append(card)
            self.combatants[card] = player

    def commenceBattle(self):
        shotsFired = sorted(self.combatants.keys(), reverse=True)

        self.writeBattleSummary(shotsFired)

        winner = self.combatants.get(shotsFired[0])
        winningCombatant = self.confirmVictory(winner, shotsFired)
        self.awardSpoils( winningCombatant)

        return winningCombatant

    def confirmVictory(self, winningPlayer, shotsFired):
        tiedCombatants = []
        for card in shotsFired[1:]:
            value1 = shotsFired[0].value
            value2 = card.value
            if value1 == value2:
                tiedCombatants.append(self.combatants.get(card))

        if len(tiedCombatants) > 0 :
            self.writeTieBreaker()
            tiedCombatants.append(winningPlayer)
            tieBreakerBg = BattleGround()
            for combatant in tiedCombatants:
                tieBreakerBg.addCombatant(combatant)
            winningPlayer = tieBreakerBg.commenceBattle()

        return winningPlayer

    def awardSpoils(self, victor):
        victor.addWin(self.playedCards)

    def writeTieBreaker(self):
        print ('!!!!!----- TIED BATTLE -----!!!!!')
        print ('              FIGHT!')

    def writeBattleSummary(self, shotsFired):
        for shot in shotsFired:
            print( self.combatants.get(shot).playerName + ':', 'Card:', shot.suit, shot.face )

    def writeWinnerSummary(self, winner):
        print('Winner:', winner.playerName + '!')