class Card:

    def __init__(self, suit, face, value):
        self.suit = suit
        self.face = face
        self.value = value

    def getFace(self):
        return self.face

    def getValue(self):
        return self.value

    def getSuit(self):
        return self.suit

    def __lt__(self, card):
        return self.value < card.getValue()
    def __gt__(self, card):
        return self.value > card.getValue()
    def __eq__(self, card):
        return self.value == card.getValue()
    def __hash__(self):
        return hash(self.value)
